<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exads\Client; use App\Campaign;
use App\Variation; use App\Zone;

class ExoclickController extends Controller
{
    public function index(Request $request, Client $client)
    {
        
        // $data = $client->campaigns->show(3868793,['campaignid' => 3868793]);
        $campaigns = $client->campaigns->all();
        // dd($campaigns);
        // dd($data);

        return view('index')->with('campaigns', $campaigns["result"]);
    }

    public function store(Request $request, Client $client){
        
        $data = $client->campaigns->show($request->input("campaignid"), ["campaignid" => $request->input("campaignid")])["result"];
        // dd($data);
        $data_campaign = $data["campaign"];
        $data_variation = $data["variations"];
        $data_zones = $data["zones"];
        $data_zone_targeting = $data["zone_targeting"];

        $campaign = Campaign::create([
            "id_campaign" => $data_campaign["id"], 
            "name" => $data_campaign["name"], 
            "type" => $data_campaign["advertiser_ad_type_label"],
            "zone_targeting_type" => $data_zone_targeting["type"]
            ]);
            // dd($campaign->id);

        foreach($data_variation as $variation){

            $campaign->variation()->create([
                "idvariation" => $variation["idvariation"], 
                "idvariations_file_url" => 
                $variation["idvariations_file"] == 0 ? 
                $variation["idvariations_url"] : $variation["idvariations_file"],
                "imageurl_url" => 
                $variation["imgurl"] != "" ? $variation["imgurl"] : $variation["url"] 

            ]);
        }

        foreach($data_zones as $zone){

            $campaign->zones()->create([

                "idzones" => $zone["idzone"],
                "price" =>  $zone["price"] == null ? "" : $zone["price"]
            
            ]);
        }

            return redirect("/");

    
    }

    // public function test($id, Request $request, Client $client){

    //     $data = $client->campaigns->show($id,['campaignid' => $id])["result"];
        

    //          dd($data["zones"]);


    // }

    public function showAll(){

        $campaigns = Campaign::all();

        return view("data_save")->with(["campaigns" => $campaigns]);
    }

    public function show($id, Request $request){
        dd($id);
    }
}
