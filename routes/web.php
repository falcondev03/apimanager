<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'ExoclickController@index');
Route::get("/saves", "ExoclickController@showAll")->name("listAll");
Route::get("/test/{id}", "ExoclickController@test")->name("test");
Route::get("/campaign/{id}", "ExoclickController@show")->name("campaign_show");
Route::post('/campaigns', 'ExoclickController@store')->name("save");

// Route::get("/db/install/{nombre}", function($nombre){
//     if($nombre == "ReneGarcia"){
        
//     }
// });
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
